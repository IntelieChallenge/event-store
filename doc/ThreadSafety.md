# Concurrency Model

It is reasonable to adopt a system of multiple readers and one writer, since the
used C++ STL containers (`std::unordered_map` and `std::multiset`) are safe
under these restrictions. Each container instance has its own `Mutex` as does
each iterator object and event store iterators list.

Events can be deleted while others iterators have it as current and accessing
invalid STL iterators is undefined behavior. The iterators list serves the
purpose of being a way of marking invalid iterators.

The store is thread safe as long as the same iterator doesn't get used by
different threads, in this case the user will have to provide it's own locking.

## Analysis tools

### Thread safety analysis

Clang has support to a compile-time thread safety check. This tool was used to
validate the code design and prevent data races and deadlocks. To use it
functions and classes need to have special compiler attributes as well as the
mutex classes that need to be wrapped. The STL containers were wrapped as well
to provide accurate resource needs to the tool. More on clang's thread safety
analysis can be found in the official
[documentation](https://clang.llvm.org/docs/ThreadSafetyAnalysis.html) and this
reference
[article](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/42958.pdf).

### Thread sanitizer

The thread safety analysis has its limitations and is not enough to guarantee
the program safety. For detecting additional deadlocks and data races the thread
sanitizer was used in the concurrency tests. This tool detects concurrency
problems at run time. A target was created to spawn threads with multiple
functionalities and attempt to force a problem. Although this test is not
deterministic, if ran a sufficient amount of times it can deliver consistent
results.
More on thread sanitizer available at the official
[documentation](https://clang.llvm.org/docs/ThreadSanitizer.html)

## Possible improvements

The iterators list design could be replaced by developing a thread safe iterator
class. This could be done by adding atomic reference counters (lock-free) at the
stored events so they get removed only when the counter reaches zero. This
implementation, however, is more difficult to pull off correctly and demands
more development time. This would make the overall design more encapsulated and
could bring multi-thread performance improvements (lock-free is not necessarily
faster, there is overhead to each event and more memory is allocated).

If we are certain to have the code running in a multi-core environment,
spinlocks would improve the overall performance.
