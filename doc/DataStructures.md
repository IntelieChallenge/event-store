# Data Structures

The problem has some requirements that are relevant to the design choice:

- Each event can only be of one type.
- The event type serves the purpose of classification. It is reasonable that
  events of the same type are stored together.
- Events must be ordered by timestamps so the user can iterate over them.

Given these requirements/qualities, the overall store structure was conceived.
This structure consists in for each event type having a unique container for
timestamps, storing in the actual data structure only the timestamps. The
following figure illustrates this design.

![](EventStoreDiagram.png)

## Event types and timestamps mapping

The problem doesn't require ordering event types or being able to iterate over
them. What is important is that, given a type, the access to its timestamps
container occurs with low cost. For that a good solution is to produce a hash
table, mapping the types at their respective containers. The complexity, in this
case, is **O(1)** for all the required operations (insert, delete, search). The
C++ `std::unordered_map` container does exactly that, with a nice interface,
being the best choice here.

## Timestamps data structure

The timestamps data structure should follow these requisites:

- Be ordered as it must support iterators.

- Have a low insertion by value cost, for ordered structures the complexity
  should be around **O(log N)**.

- Have a low access through incrementing/decrementing iterator cost. We should
  be able to move from an element to another without transversing an arbitrary
  number of nodes in between, i.e. having a complexity of **O(1)**.

- Have a low deletion by iterator cost.

Comparing some of the most used data structures, according to the chart below,
the best choices are the variations of the binary search tree.

![](BigOChart.png)

The C++ `std::set` and `std::multiset` are most often implemented as threaded
red black trees, but almost any other form of balanced tree (e.g., AVL tree,
B-tree) could be used instead. Regardless of the form the balancing takes, the
tree does have to be threaded (i.e., incrementing or decrementing an iterator is
required to have constant complexity).

For these containers we are guaranteed to have the following complexity times:

Operation | Complexity
----------|-----------
Insertion | O(log N)
Search    | O(log N)
Removal   | O(log N)
Iterating | O(1)

As we can have events with the same timestamps the `std::multiset` is the most
suitable choice.

## Exceptions

Operations that involve insertion, deletion, re-balancing can throw in the
chosen containers, however the data structure is never affected by it. As long
as the user handles standard exceptions the event store will be safe. The
handling is not necessary in the store itself as we don't intend to propagate
exceptions through threads.

## Performance tests

Some performance tests were made for reassuring the complexity analysis, the
following graphs represents time for a new operation to be complete versus
number of events in the store.

### Insertion

![](Insertion.png)

![](Insertion-logscale.png)

### Deletion

![](Deletion.png)

![](Deletion-logscale.png)

### Query

![](Query.png)

![](Query-logscale.png)
