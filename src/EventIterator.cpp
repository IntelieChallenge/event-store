#include <EventIterator.hpp>
#include <Exceptions.hpp>

void EventIterator::init(Event::Timestamp_t const &StartTime) {
  // Checking if the type exists in the store. The iterator becomes invalid if
  // it doesn't.
  SharedLock TypeSharedLock(Store.TypeSet.SelfLock);
  try {
    Store.TypeSet.at(Type);
  } catch (std::out_of_range) {
    UniqueLock SelfUniqueLock(SelfLock);
    IsValid = false;
    return;
  }
  SharedLock TimestampSharedLock(Store.TypeSet.at(Type)->SelfLock);
  UniqueLock ItListUniqueLock(CurrentIterators.SelfLock);
  CurrentIterators.emplace(this);
  UniqueLock SelfUniqueLock(SelfLock);
  // 'It' becomes an interator to the first event of the query
  It = Store.TypeSet.at(Type)->lower_bound(StartTime);
  if (It == Store.TypeSet.at(Type)->end()) {
    // Means that there isn't an event that satisfies the query. The iterator
    // becomes invalid.
    IsValid = false;
    return;
  }
  if (*It >= EndTime) {
    // Means that the first event with timestamp equal ow greater than StartTime
    // has a timestamp that exceeds the query limit. The iterator becomes
    // invalid.
    IsValid = false;
    return;
  }
}

void EventIterator::terminate() {
  UniqueLock ItListUniqueLock(CurrentIterators.SelfLock);
  CurrentIterators.erase(this);
}

Multiset<EventIterator *> EventIterator::CurrentIterators;

EventIterator::EventIterator(EventStore &Store, Event::Type_t const &Type,
                             Event::Timestamp_t const StartTime,
                             Event::Timestamp_t const EndTime)
    : Type(Type), EndTime(EndTime), Store(Store) {
  init(StartTime);
}

EventIterator::~EventIterator() { terminate(); }

bool const &EventIterator::moveNext() noexcept {
  // Fast checking for validity
  SharedLock SelfSharedLock(SelfLock);
  if (IsValid == false) {
    return IsValid;
  }
  SelfSharedLock.unlock();
  SharedLock TypeSharedLock(Store.TypeSet.SelfLock);
  try {
    Store.TypeSet.at(Type);
  } catch (std::out_of_range) {
    UniqueLock SelfUniqueLock(SelfLock);
    IsValid = false;
    return IsValid;
  }
  SharedLock TimestampSharedLock(Store.TypeSet.at(Type)->SelfLock);
  UniqueLock SelfUniqueLock(SelfLock);
  if (IsValid == false) {
    return IsValid;
  }
  if (MoveNextCalled == false) {
    // First event. It is already at the right position.
    MoveNextCalled = true;
  } else if (CurrentHasBeenRemoved == true) {
    /*
    The previously current event was removed by an EventIterator instance
    and we are already at the next valid event (or End).
    */
    CurrentHasBeenRemoved = false;
  } else {
    ++It;
  }
  if (It == Store.TypeSet.at(Type)->end()) {
    IsValid = false;
  } else if ((*It) >= EndTime) {
    IsValid = false;
  }
  if (IsValid == true) {
    CurrentTimestamp = *It;
  }
  return IsValid;
}

std::unique_ptr<Event> EventIterator::current() {
  SharedLock SelfSharedLock(SelfLock);
  if (IsValid == false || MoveNextCalled == false) {
    throw IllegalStateException();
  }
  /*
   It is important to make a new object and not declare it as a data member of
   the class. An user can get multiple events in a query and we shouldn't
   invalidate any of them, this responsability is of a superior scope.
   Also the event returned was retrieved when we moved to it, so if it gets
   removed we don't access an invalid container iterator.
  */
  return std::make_unique<Event>(Type, CurrentTimestamp);
}

void EventIterator::remove() {
  SharedLock SelfSharedLock(SelfLock);
  if (IsValid == false || MoveNextCalled == false) {
    throw IllegalStateException();
  } else if (CurrentHasBeenRemoved == true) {
    /*
     The undesired event was already removed. Also we shouldn't remove the
     next event until the user explicitly tells us to (i.e. calls moveNext and
     then remove). It is the expected behaviour just to early return from this
     function as it is already successful.
    */
    return;
  }
  auto ToRemove = It;
  SelfSharedLock.unlock();
  SharedLock TypeSharedLock(Store.TypeSet.SelfLock);
  try {
    Store.TypeSet.at(Type);
  } catch (std::out_of_range) {
    /*
     As this event type was completely removed we can be assure that this
     particular event doesn't exits anymore. It is the expected behaviour
     just to early return from this function as it is already successful.
    */
    return;
  }
  UniqueLock TimestampUniqueLock(Store.TypeSet.at(Type)->SelfLock);
  UniqueLock ItListUniqueLock(CurrentIterators.SelfLock);
  SharedLock DoubleCheckSelfSharedLock(SelfLock);
  // Another remove call could have alrady removed this event
  if (CurrentHasBeenRemoved == true) {
    return;
  }
  DoubleCheckSelfSharedLock.unlock();
  // Updating every other iterator at the same event. Avoids access to an
  // invalid iterator (when it gets removed it becomes invalid).
  for (auto Iterator : CurrentIterators) {
    UniqueLock ItListIteratorUniqueLock(Iterator->SelfLock);
    if (Iterator->It == ToRemove) {
      Iterator->CurrentHasBeenRemoved = true;
      ++(Iterator->It);
    }
  }
  Store.TypeSet.at(Type)->erase(ToRemove);
}
