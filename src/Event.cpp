#include <Event.hpp>

Event::Event(Type_t Type, Timestamp_t Timestamp) noexcept
    : Type(Type), Timestamp(Timestamp) {}

Event::Type_t const &Event::type() const noexcept { return Type; }

Event::Timestamp_t const &Event::timestamp() const noexcept {
  return Timestamp;
}
