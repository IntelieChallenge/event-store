#include <EventIterator.hpp>
#include <EventStore.hpp>

void EventStore::insert(Event NewEvent) {
  UniqueLock TypeUniqueLock(TypeSet.SelfLock);
  auto It = TypeSet.find(NewEvent.type());
  if (It == TypeSet.end()) {
    // The event type doesn't exists in the store, we need to insert it
    TypeSet.emplace(NewEvent.type(), std::make_unique<TimestampSet_t>());
  }
  UniqueLock TimestampUniqueLock(TypeSet.at(NewEvent.type())->SelfLock);
  TypeSet.at(NewEvent.type())->emplace(NewEvent.timestamp());
}

void EventStore::removeAll(Event::Type_t Type) noexcept {
  // Checking if the removed type exists in the store
  UniqueLock TypeUniqueLock(TypeSet.SelfLock);
  try {
    TypeSet.at(Type);
  } catch (std::out_of_range) {
    // If doesn't there is no need for removal
    return;
  }
  TypeSet.erase(Type);
}

std::unique_ptr<EventIterator> EventStore::query(Event::Type_t Type,
                                                 Event::Timestamp_t StartTime,
                                                 Event::Timestamp_t EndTime) {
  return std::make_unique<EventIterator>(*this, Type, StartTime, EndTime);
}
