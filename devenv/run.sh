#!/bin/bash

case $1 in
    "prepare")
        docker-compose build
        docker-compose run -u $UID:$GROUPS --rm event-store-prepare
        ;;
    "test")
        docker-compose run -u $UID:$GROUPS --rm event-store-test
        ;;
    "performance")
        docker-compose run -u $UID:$GROUPS --rm event-store-performance
        ;;
    "concurrency")
        docker-compose run -u $UID:$GROUPS --rm event-store-concurrency
        ;;
    *)
        echo "Usage:    ./run [OPTIONS]"
        echo ""
        echo "Options:"
        echo ""
        echo "  prepare         Create a build environment"
        echo "  test            Build and run unit and integration tests"
        echo "  performance     Build and run performance tests"
        echo "  concurrency     Build and run concurrency tests"
        echo ""
        exit 1
        ;;
esac
