# style
set terminal pngcairo enhanced font 'Verdana,9'
set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

# common
set ylabel 'Operation time, ms'

# Insertion
# Normal axes
set output 'Insertion.png'
set title 'Insertion performance'
set xlabel 'Number of Events'
p 'insertion.dat' with dots lc rgb '#8b1a0e' title 'Data'
# logscaled
set output 'Insertion-logscale.png'
set title 'Insertion performance'
set logscale x 10
set xlabel 'Number of Events (log scale)'
p 'insertion.dat' with dots lc rgb '#8b1a0e' title 'Data'

# Query
# Normal axes
set output 'Query.png'
set title 'Query performance'
unset logscale
set xlabel 'Number of Events'
p 'query.dat' with dots lc rgb '#8b1a0e' title 'Data'
# logscaled
set output 'Query-logscale.png'
set title 'Query performance'
set logscale x 10
set xlabel 'Number of Events (log scale)'
p 'query.dat' with dots lc rgb '#8b1a0e' title 'Data'

# Deletion
# Normal axes
set output 'Deletion.png'
set title 'Deletion performance'
unset logscale
set xlabel 'Number of Events'
p 'deletion.dat' with dots lc rgb '#8b1a0e' title 'Data'
# logscale
set output 'Deletion-logscale.png'
set title 'Deletion performance'
set logscale x 10
set xlabel 'Number of Events (log scale)'
p 'deletion.dat' with dots lc rgb '#8b1a0e' title 'Data'
