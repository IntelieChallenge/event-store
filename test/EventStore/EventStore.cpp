#include <EventIterator.hpp>
#include <EventStore.hpp>
#include <array>
#include <catch.hpp>
#include <limits>
#include <random>

SCENARIO("EventStore 'insert' method validation") {
  std::random_device RD;
  GIVEN("An event store object is created") {
    EventStore TestStore;
    WHEN("We insert a new event") {
      auto constexpr TestType = "TestType";
      auto const Timestamp = RD();
      TestStore.insert(Event(TestType, Timestamp));
      THEN("We can get that event through a query") {
        auto TestQuery = TestStore.query(TestType, Timestamp, Timestamp + 1);
        REQUIRE(TestQuery->moveNext() == true);
        REQUIRE(TestQuery->current()->type() == TestType);
        REQUIRE(TestQuery->current()->timestamp() == Timestamp);
      }
    }
    WHEN("We don't insert any events") {
      THEN("We can't get anything through a query") {
        auto constexpr TestType = "TestType";
        auto TestQuery =
            TestStore.query(TestType, std::numeric_limits<long>::lowest(),
                            std::numeric_limits<long>::max());
        REQUIRE(TestQuery->moveNext() == false);
      }
    }
    WHEN("We insert multiple events of the same type") {
      auto constexpr TestType = "TestType";
      auto constexpr NumberOfEvents = 10000;
      std::array<Event::Timestamp_t, NumberOfEvents> Timestamps;
      for (auto &Timestamp : Timestamps) {
        Timestamp = RD();
        TestStore.insert(Event(TestType, Timestamp));
      }
      THEN("We can get all of them through a query for that type and they are "
           "ordered") {
        std::sort(Timestamps.begin(), Timestamps.end());
        auto TestQuery = TestStore.query(TestType, *Timestamps.cbegin(),
                                         *Timestamps.crbegin() + 1);
        for (auto const &Timestamp : Timestamps) {
          REQUIRE(TestQuery->moveNext() == true);
          REQUIRE(TestQuery->current()->type() == TestType);
          REQUIRE(TestQuery->current()->timestamp() == Timestamp);
        }
      }
    }
    WHEN("We insert multiple events of two different types") {
      auto constexpr TestType = "TestType";
      auto constexpr TestType2 = "TestType2";
      auto constexpr NumberOfEvents = 10000;
      std::array<Event::Timestamp_t, NumberOfEvents> Timestamps;
      for (auto &Timestamp : Timestamps) {
        Timestamp = RD();
        TestStore.insert(Event(TestType, Timestamp));
      }
      std::array<Event::Timestamp_t, NumberOfEvents> Timestamps2;
      for (auto &Timestamp : Timestamps2) {
        Timestamp = RD();
        TestStore.insert(Event(TestType2, Timestamp));
      }
      std::sort(Timestamps.begin(), Timestamps.end());
      std::sort(Timestamps2.begin(), Timestamps2.end());
      THEN("We can get all of them through two different queries") {
        auto TestQuery = TestStore.query(TestType, *Timestamps.cbegin(),
                                         *Timestamps.crbegin() + 1);
        auto TestQuery2 = TestStore.query(TestType2, *Timestamps2.cbegin(),
                                          *Timestamps2.crbegin() + 1);
        for (auto const &Timestamp : Timestamps) {
          REQUIRE(TestQuery->moveNext() == true);
          REQUIRE(TestQuery->current()->type() == TestType);
          REQUIRE(TestQuery->current()->timestamp() == Timestamp);
        }
        for (auto const &Timestamp : Timestamps2) {
          REQUIRE(TestQuery2->moveNext() == true);
          REQUIRE(TestQuery2->current()->type() == TestType2);
          REQUIRE(TestQuery2->current()->timestamp() == Timestamp);
        }
      }
    }
  }
}

SCENARIO("EventStore 'removeAll' method validation") {
  std::random_device RD;
  GIVEN("An event store object is created") {
    EventStore TestStore;
    WHEN("We insert multiple events of the same type") {
      auto constexpr TestType = "TestType";
      auto constexpr NumberOfEvents = 10000;
      for (auto EventNumber = 0; EventNumber < NumberOfEvents; ++EventNumber) {
        TestStore.insert(Event(TestType, RD()));
      }
      AND_WHEN("We remove all of them") {
        TestStore.removeAll(TestType);
        THEN("We can't get anything through a query for that type") {
          auto TestQuery =
              TestStore.query(TestType, std::numeric_limits<long>::lowest(),
                              std::numeric_limits<long>::max());
          REQUIRE(TestQuery->moveNext() == false);
        }
      }
    }
    WHEN("We don't insert any events") {
      AND_WHEN("We attempt to remove all events for a type") {
        auto constexpr TestType = "TestType";
        TestStore.removeAll(TestType);
        THEN("We can't get anything through a query for that type") {
          auto TestQuery =
              TestStore.query(TestType, std::numeric_limits<long>::lowest(),
                              std::numeric_limits<long>::max());
          REQUIRE(TestQuery->moveNext() == false);
        }
      }
    }
  }
}

SCENARIO("EventStore 'query' method validation") {
  std::random_device RD;
  GIVEN("An EventStore object is created") {
    EventStore TestStore;
    WHEN("We insert multiple events of the same type, with sequential "
         "timestamps") {
      auto constexpr TestType = "TestType";
      auto constexpr NumberOfEvents = 10000;
      std::array<Event::Timestamp_t, NumberOfEvents> Timestamps;
      for (auto &Timestamp : Timestamps) {
        Timestamp = RD();
        TestStore.insert(Event(TestType, Timestamp));
      }
      std::sort(Timestamps.begin(), Timestamps.end());
      AND_WHEN("We query some of them, setting the StartTime and EndTime as "
               "two random created events timestamps") {
        // StartTime is in the first 50% of the event set
        auto const StartTimeIndex = RD() % NumberOfEvents / 2;
        // Guaranteeing at least 10% of the event set range
        auto EndTimeIndex = StartTimeIndex;
        while (EndTimeIndex - StartTimeIndex < NumberOfEvents / 10) {
          EndTimeIndex =
              StartTimeIndex + RD() % (NumberOfEvents - StartTimeIndex);
        }
        auto TestQuery = TestStore.query(TestType, Timestamps[StartTimeIndex],
                                         Timestamps[EndTimeIndex]);
        THEN("We can get the Events in that range, ordered, excluding the last "
             "one corresponding to the EndTime") {
          for (auto Index = StartTimeIndex; Index < EndTimeIndex; ++Index) {
            REQUIRE(TestQuery->moveNext() == true);
            REQUIRE(TestQuery->current()->type() == TestType);
            REQUIRE(TestQuery->current()->timestamp() == Timestamps[Index]);
          }
          REQUIRE(TestQuery->moveNext() == false);
        }
      }
      AND_WHEN(
          "We query some of them, setting the StartTime and EndTime as two "
          "random event timestamps, with StartTime higher than the EndTime") {
        // EndTime is in the first 50% of the event set
        auto const EndTimeIndex = RD() % NumberOfEvents / 2;
        auto StartTimeIndex = EndTimeIndex;
        // Guaranteeing at least 10% of the event set range
        while (StartTimeIndex - EndTimeIndex < NumberOfEvents / 10) {
          StartTimeIndex =
              EndTimeIndex + RD() % (NumberOfEvents - EndTimeIndex);
        }
        auto TestQuery = TestStore.query(TestType, Timestamps[StartTimeIndex],
                                         Timestamps[EndTimeIndex]);
        THEN("The query is empty") { REQUIRE(TestQuery->moveNext() == false); }
      }
      AND_WHEN("We attempt to query one Event setting the StartTime and "
               "EndTime equal to its Timestamp") {
        auto const RandomTimestamp = Timestamps[RD() % NumberOfEvents];
        auto TestQuery =
            TestStore.query(TestType, RandomTimestamp, RandomTimestamp);
        THEN("The query is empty") { REQUIRE(TestQuery->moveNext() == false); }
      }
      AND_WHEN("We attempt to query an inexistent Type") {
        auto TestQuery = TestStore.query("InexistentType",
                                         std::numeric_limits<long>::lowest(),
                                         std::numeric_limits<long>::max());
        THEN("The query is empty") { REQUIRE(TestQuery->moveNext() == false); }
      }
    }
  }
}
