#include <EventIterator.hpp>
#include <EventStore.hpp>
#include <array>
#include <iostream>
#include <limits>
#include <random>
#include <thread>

class SelfJoinThread {
  std::thread Thread;

public:
  template <class Fn, class... Args>
  explicit SelfJoinThread(Fn &&fn, Args &&... args) : Thread(fn, args...) {}
  ~SelfJoinThread() { Thread.join(); }
};

EventStore GlobalStore;
std::array<Event::Type_t, 3> const TestTypes = {{"Type1", "Type2", "Type3"}};

void ESinsert() {
  constexpr auto InsertionsPerType = 1000;
  std::random_device RD;
  for (auto TestType : TestTypes) {
    for (auto Iteration = InsertionsPerType; Iteration > 0; --Iteration) {
      GlobalStore.insert(Event(TestType, RD()));
    }
  }
}
void ESremove() {
  constexpr auto MaxRemovalsPerType = 1000;
  for (auto TestType : TestTypes) {
    auto Iterator = GlobalStore.query(
        TestType, std::numeric_limits<Event::Timestamp_t>::lowest(),
        std::numeric_limits<Event::Timestamp_t>::max());
    for (auto Iteration = MaxRemovalsPerType; Iteration > 0; --Iteration) {
      if (Iterator->moveNext() == true) {
        Iterator->remove();
      } else {
        break;
      }
    }
  }
}

void EScurrent() {
  constexpr auto MaxCurrentCallsPerType = 1000;
  for (auto TestType : TestTypes) {
    auto Iterator = GlobalStore.query(
        TestType, std::numeric_limits<Event::Timestamp_t>::lowest(),
        std::numeric_limits<Event::Timestamp_t>::max());
    for (auto Iteration = MaxCurrentCallsPerType; Iteration > 0; --Iteration) {
      if (Iterator->moveNext() == true) {
        Iterator->current();
      }
    }
  }
}

int main(void) {
  auto constexpr NumberOfTestIterations = 100;
  for (auto TestIteration = 0; TestIteration < NumberOfTestIterations;
       ++TestIteration) {
    ESinsert();
    ESinsert();
    ESinsert();
    std::cout << TestIteration + 1 << " (out of " << NumberOfTestIterations
              << ") Running concurrency tests...\n";
    SelfJoinThread t1(ESinsert);
    SelfJoinThread t2(ESinsert);
    SelfJoinThread t3(ESinsert);
    SelfJoinThread t4(ESremove);
    SelfJoinThread t5(ESremove);
    SelfJoinThread t6(ESremove);
    SelfJoinThread t7(EScurrent);
    SelfJoinThread t8(EScurrent);
    SelfJoinThread t9(EScurrent);
    SelfJoinThread t10(
        [&]() { GlobalStore.removeAll(TestTypes[TestIteration % 3]); });
  }
}
