#include <EventIterator.hpp>
#include <EventStore.hpp>
#include <catch.hpp>
#include <limits>
#include <random>

SCENARIO("EventStore's 'insert' method integration") {
  std::random_device RD;
  GIVEN("An event store with an event") {
    EventStore TestStore;
    auto constexpr TestType = "TestType";
    TestStore.insert(Event(TestType, RD()));
    WHEN("We call 'removeAll' for that type") {
      TestStore.removeAll(TestType);
      AND_WHEN("We insert another element of that type") {
        auto const Timestamp = RD();
        TestStore.insert(Event(TestType, Timestamp));
        THEN("We can get that event through a query") {
          auto TestQuery = TestStore.query(TestType, Timestamp, Timestamp + 1);
          REQUIRE(TestQuery->moveNext() == true);
          REQUIRE(TestQuery->current()->type() == TestType);
          REQUIRE(TestQuery->current()->timestamp() == Timestamp);
        }
      }
    }
    WHEN("We remove that event through an iterator") {
      EventIterator TestIterator(TestStore, TestType,
                                 std::numeric_limits<long>::lowest(),
                                 std::numeric_limits<long>::max());
      TestIterator.moveNext();
      TestIterator.remove();
      AND_WHEN("We insert another element of that type") {
        auto const Timestamp = RD();
        TestStore.insert(Event(TestType, Timestamp));
        THEN("We can get that event through a query") {
          auto TestQuery = TestStore.query(TestType, Timestamp, Timestamp + 1);
          REQUIRE(TestQuery->moveNext() == true);
          REQUIRE(TestQuery->current()->type() == TestType);
          REQUIRE(TestQuery->current()->timestamp() == Timestamp);
        }
      }
    }
  }
}

SCENARIO("EventIterator's methods integration with 'removeAll' function") {
  EventStore TestStore;
  GIVEN("A valid event iterator") {
    auto constexpr TestType = "TestType";
    auto constexpr StartTime = 0;
    auto constexpr EndTime = 1000;
    for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
      TestStore.insert(Event(TestType, Timestamp));
    }
    auto TestEventIterator = TestStore.query(TestType, StartTime, EndTime);
    WHEN("We move to the first iterator event and then use removeAll for that "
         "type") {
      TestEventIterator->moveNext();
      TestStore.removeAll(TestType);
      AND_WHEN("We attempt to get the current event") {
        auto CurrentEvent = TestEventIterator->current();
        THEN("The operation is successful") {
          REQUIRE(CurrentEvent->type() == TestType);
          REQUIRE(CurrentEvent->timestamp() == StartTime);
        }
        AND_THEN("'moveNext' returns false") {
          REQUIRE(TestEventIterator->moveNext() == false);
        }
      }
      AND_WHEN("We attempt to remove the current event") {
        TestEventIterator->remove();
        THEN("No errors are generated and 'moveNext' returns false") {
          REQUIRE(TestEventIterator->moveNext() == false);
        }
      }
      AND_WHEN("We attempt to move to the next event") {
        auto MoveNextReturn = TestEventIterator->moveNext();
        THEN("'moveNext' returns false") { REQUIRE(MoveNextReturn == false); }
      }
    }
  }
}

SCENARIO("EventIterator's methods integration with 'insert' function") {
  EventStore TestStore;
  std::random_device RD;
  GIVEN("An EventIterator with its EndTime between existing timestamps, "
        "matching none of them") {
    auto constexpr TestType = "TestType";
    TestStore.insert(Event(TestType, 0));
    TestStore.insert(Event(TestType, 1));
    TestStore.insert(Event(TestType, 4));
    EventIterator TestEventIterator(TestStore, TestType, 0, 3);
    WHEN("We insert an event with timestamp between the EndTime and the last "
         "query event") {
      TestStore.insert(Event(TestType, 2));
      THEN("That event is reached by the iterator") {
        TestEventIterator.moveNext();
        TestEventIterator.moveNext();
        REQUIRE(TestEventIterator.moveNext() == true);
        REQUIRE(TestEventIterator.current()->type() == TestType);
        REQUIRE(TestEventIterator.current()->timestamp() == 2);
      }
    }
  }
}
