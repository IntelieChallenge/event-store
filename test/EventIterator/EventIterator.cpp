#include <EventIterator.hpp>
#include <Exceptions.hpp>
#include <catch.hpp>
#include <random>
#include <vector>

SCENARIO("EventIterator 'moveNext' method validation") {
  EventStore TestStore;
  GIVEN("A valid event iterator") {
    auto constexpr TestType = "TestType";
    auto constexpr StartTime = 0;
    auto constexpr EndTime = 1000;
    for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
      TestStore.insert(Event(TestType, Timestamp));
    }
    EventIterator TestEventIterator(TestStore, TestType, StartTime, EndTime);
    WHEN("We attempt to move to the first event") {
      TestEventIterator.moveNext();
      THEN("The current event is the correct one") {
        REQUIRE(TestEventIterator.current()->timestamp() == StartTime);
      }
    }
    WHEN("We run through all of the events") {
      THEN("We can get all of them correctly") {
        for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
          REQUIRE(TestEventIterator.moveNext() == true);
          REQUIRE(TestEventIterator.current()->timestamp() == Timestamp);
        }
      }
    }
    WHEN("We try to move after the last element") {
      for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
        TestEventIterator.moveNext();
      }
      auto MoveNextReturn = TestEventIterator.moveNext();
      THEN("We get a fail status from 'moveNext'") {
        REQUIRE(MoveNextReturn == false);
      }
    }
    WHEN("We remove the first event and attempt to move to the next one") {
      TestEventIterator.moveNext();
      TestEventIterator.remove();
      auto MoveNextReturn = TestEventIterator.moveNext();
      THEN("The 'moveNext' method is successful") {
        REQUIRE(MoveNextReturn == true);
        AND_THEN("The current event is correct") {
          REQUIRE(TestEventIterator.current()->type() == TestType);
          REQUIRE(TestEventIterator.current()->timestamp() == StartTime + 1);
        }
      }
    }
  }
  GIVEN("An empty event iterator") {
    auto constexpr TestType = "TestType";
    EventIterator TestEventIterator(TestStore, TestType, 0, 0);
    WHEN("We attempt to move to the first event") {
      auto MoveNextReturn = TestEventIterator.moveNext();
      THEN("We get a fail status from moveNext") {
        REQUIRE(MoveNextReturn == false);
      }
    }
  }
}

SCENARIO("EventIterator 'current' method validation") {
  EventStore TestStore;
  GIVEN("A valid EventIterator") {
    auto constexpr TestType = "TestType";
    auto constexpr StartTime = 0;
    auto constexpr EndTime = 1000;
    for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
      TestStore.insert(Event(TestType, Timestamp));
    }
    EventIterator TestEventIterator(TestStore, TestType, StartTime, EndTime);
    WHEN("We attempt to get the current event without ever calling moveNext") {
      std::string ExceptionMessage;
      try {
        TestEventIterator.current();
      } catch (std::exception const &Exception) {
        ExceptionMessage = Exception.what();
      }
      THEN("We get the IllegalStateException") {
        REQUIRE(ExceptionMessage == IllegalStateException::Message);
      }
    }
    WHEN("We call moveNext until it returns false, and then attempt to get the "
         "current event") {
      for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
        TestEventIterator.moveNext();
      }
      REQUIRE(TestEventIterator.moveNext() == false);
      std::string ExceptionMessage;
      try {
        TestEventIterator.current();
      } catch (std::exception const &Exception) {
        ExceptionMessage = Exception.what();
      }
      THEN("We get the IllegalStateException") {
        REQUIRE(ExceptionMessage == IllegalStateException::Message);
      }
    }
    WHEN("We properly use current to get all query events") {
      std::vector<std::unique_ptr<Event>> QueryEvents;
      for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
        TestEventIterator.moveNext();
        QueryEvents.push_back(TestEventIterator.current());
      }
      THEN("All the extracted events match the inserted ones") {
        auto ExpectedTimestamp = StartTime;
        for (auto &QueryEvent : QueryEvents) {
          REQUIRE(QueryEvent->timestamp() == ExpectedTimestamp);
          ++ExpectedTimestamp;
        }
      }
    }
    WHEN("We remove the first event and attempt to get it right after") {
      TestEventIterator.moveNext();
      TestEventIterator.remove();
      auto CurrentEvent = TestEventIterator.current();
      THEN("The 'current' method is successful") {
        REQUIRE(CurrentEvent->type() == TestType);
        REQUIRE(CurrentEvent->timestamp() == StartTime);
      }
    }
  }
  GIVEN("An empty event iterator") {
    auto constexpr TestType = "TestType";
    auto TestEventIterator = EventIterator(TestStore, TestType, 0, 0);
    WHEN("We attempt to get the first iterator event") {
      TestEventIterator.moveNext();
      std::string ExceptionMessage;
      try {
        TestEventIterator.current();
      } catch (std::exception const &Exception) {
        ExceptionMessage = Exception.what();
      }
      THEN("We get the IllegalStateException") {
        REQUIRE(ExceptionMessage == IllegalStateException::Message);
      }
    }
  }
}

SCENARIO("EventIterator 'remove' method validation") {
  EventStore TestStore;
  std::random_device RD;
  GIVEN("A valid event iterator") {
    auto constexpr TestType = "TestType";
    auto constexpr StartTime = 0;
    auto constexpr EndTime = 10;
    for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
      TestStore.insert(Event(TestType, Timestamp));
    }
    EventIterator TestEventIterator(TestStore, TestType, StartTime, EndTime);
    WHEN("We attempt to remove the current event without ever calling "
         "moveNext") {
      std::string ExceptionMessage;
      try {
        TestEventIterator.remove();
      } catch (std::exception const &Exception) {
        ExceptionMessage = Exception.what();
      }
      THEN("We get the IllegalStateException") {
        REQUIRE(ExceptionMessage == IllegalStateException::Message);
      }
    }
    WHEN("We call moveNext until it returns false, and then attempt to remove "
         "the current event") {
      for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
        TestEventIterator.moveNext();
      }
      REQUIRE(TestEventIterator.moveNext() == false);
      std::string ExceptionMessage;
      try {
        TestEventIterator.remove();
      } catch (std::exception const &Exception) {
        ExceptionMessage = Exception.what();
      }
      THEN("We get the IllegalStateException") {
        REQUIRE(ExceptionMessage == IllegalStateException::Message);
      }
    }
    WHEN("We properly attempt to remove a random event from the store") {
      auto TimestampToRemove = RD() % EndTime;
      for (long Timestamp = StartTime; Timestamp <= TimestampToRemove;
           ++Timestamp) {
        TestEventIterator.moveNext();
      }
      TestEventIterator.remove();
      THEN("We can't query that event anymore") {
        auto RemovedEventQuery =
            TestStore.query(TestType, TimestampToRemove, TimestampToRemove + 1);
        REQUIRE(RemovedEventQuery->moveNext() == false);
      }
      AND_WHEN("We attempt to remove that event again") {
        TestEventIterator.remove();
        THEN("No error is generated") {}
      }
    }
    WHEN("We properly attempt to remove the event with the lowest timestamp "
         "from the store") {
      TestEventIterator.moveNext();
      TestEventIterator.remove();
      THEN("We can't query that event anymore") {
        auto RemovedEventQuery =
            TestStore.query(TestType, StartTime, StartTime + 1);
        REQUIRE(RemovedEventQuery->moveNext() == false);
      }
    }
    WHEN("We properly attempt to remove the event with the highest timestamp "
         "from the store") {
      for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
        TestEventIterator.moveNext();
      }
      TestEventIterator.remove();
      THEN("We can't query that event anymore") {
        auto RemovedEventQuery =
            TestStore.query(TestType, EndTime - 1, EndTime);
        REQUIRE(RemovedEventQuery->moveNext() == false);
      }
    }
  }
  GIVEN("An empty EventIterator") {
    auto constexpr TestType = "TestType";
    auto TestEventIterator = EventIterator(TestStore, TestType, 0, 0);
    WHEN("We attempt to remove the first iterator event") {
      TestEventIterator.moveNext();
      std::string ExceptionMessage;
      try {
        TestEventIterator.remove();
      } catch (std::exception const &Exception) {
        ExceptionMessage = Exception.what();
      }
      THEN("We get the IllegalStateException") {
        REQUIRE(ExceptionMessage == IllegalStateException::Message);
      }
    }
  }
}
