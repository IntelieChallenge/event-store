/*
  Tests to guarantee expected behaviour when dealing with multiple iterators at
  the same time.
*/

#include <EventIterator.hpp>
#include <catch.hpp>

SCENARIO("Iterators concurrency validation") {
  EventStore TestStore;
  GIVEN("Two identic event iterators") {
    auto constexpr TestType = "TestType";
    auto constexpr StartTime = 0;
    auto constexpr EndTime = 1000;
    for (auto Timestamp = StartTime; Timestamp < EndTime; ++Timestamp) {
      TestStore.insert(Event(TestType, Timestamp));
    }
    EventIterator TestEventIterator(TestStore, TestType, StartTime, EndTime);
    EventIterator TestEventIterator2(TestStore, TestType, StartTime, EndTime);
    WHEN("They are at the first event and one of them removes that event") {
      TestEventIterator.moveNext();
      TestEventIterator2.moveNext();
      TestEventIterator.remove();
      AND_WHEN("The other one attempts to remove that event") {
        TestEventIterator2.remove();
        THEN("It doesn't modify the store, or invalidate the iterators. No "
             "errors are generated") {
          for (auto Timestamp = StartTime + 1; Timestamp < EndTime;
               ++Timestamp) {
            REQUIRE(TestEventIterator.moveNext() == true);
            REQUIRE(TestEventIterator2.moveNext() == true);
            REQUIRE(TestEventIterator.current()->type() == TestType);
            REQUIRE(TestEventIterator.current()->timestamp() == Timestamp);
            REQUIRE(TestEventIterator2.current()->type() == TestType);
            REQUIRE(TestEventIterator2.current()->timestamp() == Timestamp);
          }
        }
      }
      AND_WHEN("The other one attempts to get that event") {
        auto CurrentEvent = TestEventIterator2.current();
        THEN("It correctly gets that removed event") {
          REQUIRE(CurrentEvent->type() == TestType);
          REQUIRE(CurrentEvent->timestamp() == StartTime);
        }
        AND_THEN("It doesn't modify the store, or invalidate the iterators. No "
                 "errors are generated") {
          for (auto Timestamp = StartTime + 1; Timestamp < EndTime;
               ++Timestamp) {
            REQUIRE(TestEventIterator.moveNext() == true);
            REQUIRE(TestEventIterator2.moveNext() == true);
            REQUIRE(TestEventIterator.current()->type() == TestType);
            REQUIRE(TestEventIterator.current()->timestamp() == Timestamp);
            REQUIRE(TestEventIterator2.current()->type() == TestType);
            REQUIRE(TestEventIterator2.current()->timestamp() == Timestamp);
          }
        }
      }
    }
    WHEN("They are at the same event and one of them removes two consecutive "
         "events") {
      TestEventIterator.moveNext();
      TestEventIterator2.moveNext();
      TestEventIterator.remove();
      TestEventIterator.moveNext();
      TestEventIterator.remove();
      THEN("The other iterator next event is correct") {
        REQUIRE(TestEventIterator2.moveNext() == true);
        REQUIRE(TestEventIterator2.current()->type() == TestType);
        REQUIRE(TestEventIterator2.current()->timestamp() == StartTime + 2);
      }
    }
  }
}
