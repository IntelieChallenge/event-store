#include <Event.hpp>
#include <catch.hpp>
#include <random>

SCENARIO("Event class methods validation") {
  std::random_device RD;
  GIVEN("An Event object is created") {
    auto Timestamp = RD();
    auto TestType = "TestType";
    Event TestEvent(TestType, Timestamp);
    WHEN("We extract the object type") {
      auto TestObjectType = TestEvent.type();
      THEN("The type is correct") { REQUIRE(TestObjectType == TestType); }
    }
    WHEN("We extract the object timestamp") {
      auto TestObjectTimestamp = TestEvent.timestamp();
      THEN("The timestamp is correct") {
        REQUIRE(TestObjectTimestamp == Timestamp);
      }
    }
  }
}
