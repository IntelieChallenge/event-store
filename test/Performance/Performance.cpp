#include <EventIterator.hpp>
#include <EventStore.hpp>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <limits>
#include <random>
#include <string>

class ExecutionTime {
  std::string FileName;
  unsigned int EventNumber;
  std::chrono::time_point<std::chrono::high_resolution_clock> Start;

public:
  ExecutionTime(std::string FileName, unsigned int EventNumber)
      : FileName(FileName), EventNumber(EventNumber),
        Start(std::chrono::high_resolution_clock::now()) {}
  ~ExecutionTime() {
    auto Finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> Elapsed = Finish - Start;
    std::ofstream Output(FileName, std::ios::out | std::ios::app);
    Output << EventNumber << "\t" << Elapsed.count() << "\n";
  }
};

int main(void) {
  std::cout << "Running performance tests...\n";
  // Removing measurement files, if existent
  remove("build/performance/insertion.dat");
  remove("build/performance/query.dat");
  remove("build/performance/deletion.dat");

  auto constexpr TestType = "TestType";
  std::random_device RD;
  EventStore TestStore;
  unsigned int constexpr NumberOfEvents = 1000000;
  unsigned int constexpr NumberOfPoints = 1000000;
  static_assert(NumberOfPoints <= NumberOfEvents,
                "Number of points higher than number of events");
  // So the timestamps distribution don't get too scattered and affect
  // operations measures like query times
  unsigned int MaxTimestamp = NumberOfEvents;
  Event::Timestamp_t MaxCurrentTimestamp = 1;
  Event::Timestamp_t MinCurrentTimestamp = 0;
  for (unsigned int EventNumber = 0; EventNumber < NumberOfEvents;
       ++EventNumber) {
    // Timestamps between 0 and MaxTimestamp
    Event::Timestamp_t Timestamp = (RD() % MaxTimestamp);
    // for query intervals
    MaxCurrentTimestamp = std::max(MaxCurrentTimestamp, Timestamp);
    MinCurrentTimestamp = std::min(MaxCurrentTimestamp, Timestamp);
    // insertion
    if ((EventNumber % (NumberOfEvents / NumberOfPoints) == 0)) {
      ExecutionTime Measure("build/performance/insertion.dat", EventNumber + 1);
      TestStore.insert(Event(TestType, Timestamp));
    } else {
      TestStore.insert(Event(TestType, Timestamp));
    }
    // query
    auto TimestampRange = MaxCurrentTimestamp - MinCurrentTimestamp;
    // StartTime is in the first half of the valid range
    auto const StartTime =
        (TimestampRange == 0)
            ? MinCurrentTimestamp
            : MinCurrentTimestamp + (RD() % (TimestampRange) / 2);
    // StartTime is in the second half of the valid range
    auto EndTime = (TimestampRange == 0)
                       ? MaxCurrentTimestamp
                       : StartTime + (RD() % (TimestampRange) / 2);
    if ((EventNumber % (NumberOfEvents / NumberOfPoints) == 0)) {
      ExecutionTime Measure("build/performance/query.dat", EventNumber + 1);
      auto TestQuery = TestStore.query(TestType, StartTime, EndTime);
    }
  }

  // deletion via iterator
  auto TestQuery =
      TestStore.query(TestType, std::numeric_limits<long>::lowest(),
                      std::numeric_limits<long>::max());
  for (auto EventNumber = NumberOfEvents; EventNumber > 0; --EventNumber) {
    TestQuery->moveNext();
    if ((EventNumber % (NumberOfEvents / NumberOfPoints) == 0)) {
      ExecutionTime Measure("build/performance/deletion.dat", EventNumber);
      TestQuery->remove();
    } else {
      TestQuery->remove();
    }
  }
}
