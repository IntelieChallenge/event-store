#pragma once

#include <string>

//! Class that represents events.
class Event {

public:
  // Type definitions
  using Type_t = std::string;
  using Timestamp_t = long;

  Event(Type_t, Timestamp_t) noexcept;
  //! Gets the Event Type
  /*!
    \return the event type.
  */
  Type_t const &type() const noexcept;
  //! Gets the Event Timestamp
  /*!
    \return the event timestamp.
  */
  Timestamp_t const &timestamp() const noexcept;

private:
  Type_t const Type;
  Timestamp_t const Timestamp;
};
