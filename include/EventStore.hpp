#pragma once

#include <Containers.hpp>
#include <Event.hpp>

// Forward declaration of EventIterator class as we need this type declared to
// construct the store interface.
class EventIterator;

/*!
  This class is responsible for storing events and to provide access and control
  over the storage.
*/
class EventStore {
public:
  // Type definitions
  using TimestampSet_t = Multiset<Event::Timestamp_t>;
  using TypeSet_t =
      UnorderedMap<Event::Type_t, std::unique_ptr<TimestampSet_t>>;

  //! Stores an event.
  /*!
    \param [in] NewEvent is the event to be stored.
  */
  void insert(Event NewEvent);
  //! Removes all events of a specific type.
  /*!
    \param [in] Type is the event type to be removed from the store.
  */
  void removeAll(Event::Type_t Type) noexcept;
  //! Retrieves an iterator for events based on their type and timestamp.
  /*!
    \param [in] Type is the type we are querying for.
    \param [in] StartTime is the start timestamp (inclusive).
    \param [in] EndTime is the end timestamp (exclusive).
    \return an iterator where all its events have the same type as \p Type and
    timestamp between \p StartTime (inclusive) and \p EndTime (exclusive).
  */
  std::unique_ptr<EventIterator> query(Event::Type_t Type,
                                       Event::Timestamp_t StartTime,
                                       Event::Timestamp_t EndTime);

private:
  // Instance of the data structure
  TypeSet_t TypeSet;
  // The EventIterator class needs access to the data structure
  friend EventIterator;
};
