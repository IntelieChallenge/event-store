#include <exception>

class IllegalStateException : public std::exception {
  virtual char const *what() const throw();

public:
  constexpr static char const *Message = "IllegalState";
};
