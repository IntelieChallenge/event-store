/*!
  STL's containers wrappers, added locks and thread safety annotations.
*/

#include <Mutex.hpp>
#include <set>
#include <unordered_map>

template <typename Key, typename Value> class UnorderedMap {
  std::unordered_map<Key, Value> Instance;

public:
  SharedMutex SelfLock;

  using const_iterator =
      typename std::unordered_map<Key, Value>::const_iterator;
  auto find(Key const &K) const REQUIRES_SHARED(SelfLock) {
    return Instance.find(K);
  }
  auto end() const REQUIRES_SHARED(SelfLock) { return Instance.end(); }
  Value const &at(Key const &K) const REQUIRES_SHARED(SelfLock) {
    return Instance.at(K);
  }
  template <class... Args> void emplace(Args &&... args) REQUIRES(SelfLock) {
    Instance.emplace(std::forward<Args>(args)...);
  }
  void erase(Key K) REQUIRES(SelfLock) { Instance.erase(K); }
};

template <typename Type> class Multiset {
  std::multiset<Type> Instance;

public:
  SharedMutex SelfLock;

  using const_iterator = typename std::multiset<Type>::const_iterator;
  auto begin() const REQUIRES_SHARED(SelfLock) { return Instance.begin(); }
  auto end() const REQUIRES_SHARED(SelfLock) { return Instance.end(); }
  auto lower_bound(Type T) REQUIRES_SHARED(SelfLock) {
    return Instance.lower_bound(T);
  }
  template <class... Args> void emplace(Args &&... args) REQUIRES(SelfLock) {
    Instance.emplace(std::forward<Args>(args)...);
  }
  template <class Iterator> void erase(Iterator &&It) REQUIRES(SelfLock) {
    Instance.erase(It);
  }
};
