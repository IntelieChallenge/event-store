#pragma once

#include <EventStore.hpp>

//! An iterator over an event collection.
class EventIterator {
public:
  EventIterator(EventStore &, Event::Type_t const &, Event::Timestamp_t const,
                Event::Timestamp_t const);
  ~EventIterator();
  //! Moves the iterator to the next event, if any.
  /*!
    \return false if the iterator has reached the end, true otherwise.
  */
  bool const &moveNext() noexcept;
  //! Gets the current event ref'd by this iterator.
  /*!
    This method returns a smart pointer for a copy of the event instead of the
    actual event. The reason for this interface is that the user should have
    control over the returned object lifespan. The only note for the user here
    is to not get a lot of elements at the same storage duration as they will
    only be destroyed once their scope is done (or directly told to do so).

    \throws IllegalStateException if \p moveNext was never called or its last
    result was false.
    \return a copy of the event.
  */
  std::unique_ptr<Event> current();
  //! Remove current event from its store.
  /*!
    \throws IllegalStateException if \p moveNext was never called or its last
    result was false.
  */
  void remove();

private:
  // Constant data members. These don't need thread guarding as they can't be
  // modified after the object construction
  Event::Type_t const Type;
  Event::Timestamp_t const EndTime;
  EventStore &Store;
  // Mutex that guards non const data members of the class
  SharedMutex SelfLock;
  // State flags
  bool IsValid GUARDED_BY(SelfLock) = true;
  bool MoveNextCalled GUARDED_BY(SelfLock) = false;
  bool CurrentHasBeenRemoved GUARDED_BY(SelfLock) = false;
  // Actual container iterator
  Multiset<Event::Timestamp_t>::const_iterator GUARDED_BY(SelfLock) It;
  // Current event, updated when moveNext is called. This is necessary because
  // one can remove (through another iterator or removeAll) the current event.
  Event::Timestamp_t CurrentTimestamp GUARDED_BY(SelfLock);
  /*
   List of all the iterators
   The STL does not provide a way to check an iterator validity. We have to
   manually mark all the invalid pointers if a removal occurs. That is the
   reason we need this list.
  */
  static Multiset<EventIterator *> CurrentIterators;
  /*
    Construction and destruction functions. Thread safety analysis does not run
    through constructors and destructors.
  */
  void init(Event::Timestamp_t const &);
  void terminate();
};
