#pragma once

#include <shared_mutex>
#include <tools/ThreadSafetyAnnotations.hpp>

// NOTE: Wrappers for std::mutex and std::unique_lock are provided so that
// we can annotate them with thread safety attributes and use the
// -Wthread-safety warning with clang. The standard library types cannot be
// used directly because they do not provided the required annotations.

class CAPABILITY("mutex") SharedMutex {
public:
  SharedMutex() {}

  void lock() ACQUIRE_SHARED() { MutexInstance.lock(); }
  void unlock() RELEASE() { MutexInstance.unlock(); }
  std::shared_mutex &native_handle() { return MutexInstance; }

private:
  mutable std::shared_mutex MutexInstance;
};

class SCOPED_CAPABILITY UniqueLock {
public:
  UniqueLock(SharedMutex &MutexToLock) ACQUIRE(MutexToLock)
      : UniqueLockInstance(MutexToLock.native_handle()) {}
  ~UniqueLock() RELEASE() {}
  std::unique_lock<std::shared_mutex> &native_handle() {
    return UniqueLockInstance;
  }
  void unlock() RELEASE() { UniqueLockInstance.unlock(); }

private:
  std::unique_lock<std::shared_mutex> UniqueLockInstance;
};

class SCOPED_CAPABILITY SharedLock {
public:
  SharedLock(SharedMutex &MutexToLock) ACQUIRE_SHARED(MutexToLock)
      : SharedLockInstance(MutexToLock.native_handle()) {}
  ~SharedLock() RELEASE() {}
  std::shared_lock<std::shared_mutex> &native_handle() {
    return SharedLockInstance;
  }
  void unlock() RELEASE() { SharedLockInstance.unlock(); }

private:
  std::shared_lock<std::shared_mutex> SharedLockInstance;
};
