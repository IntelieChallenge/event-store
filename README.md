# Event Store Implementation

## Documentation

- [DataStructures](doc/DataStructures.md)
- [ThreadSafety](doc/ThreadSafety.md)

## Setting up

To compile and run tests you will need to install
[docker](https://docs.docker.com/install/) and
[docker-compose](https://docs.docker.com/compose/install/).
After installed, to set up the environment run:
```
cd devenv
./run.sh prepare
```

## Compile and run tests

### Unit tests

```
cd devenv
./run.sh test
```

### Performance tests

```
cd devenv
./run.sh performance
```

### Concurrency tests

```
cd devenv
./run.sh concurrency
```
